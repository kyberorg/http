FROM nginx:stable
COPY nginx.conf  /etc/nginx/conf.d/default.conf
COPY index.html /srv/index.html
CMD /bin/bash -c "nginx -g 'daemon off;'"
EXPOSE 8080
